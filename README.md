# JULIVING #

Repositorio para trabajos colaborativos de las materias de desarrollo de aplicaciones web y bases de datos.

El repositorio con los archivos del proyecto se encuentra [aquí](https://bitbucket.org/juliving/estrategiaculinaria "aquí").

## Repositorio con el proyecto ##
[AQUÍ](https://bitbucket.org/juliving/estrategiaculinaria "AQUÍ").

## Miembros ##

* A01206568 Juan Manuel Ledesma Rangel
* A01700457 Lino Ronaldo Contreras Gallegos
* A01700762 Ivett Núñez Martínez
