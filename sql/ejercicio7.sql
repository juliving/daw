/*

- Pel�cula (t�tulo, a�o, duraci�n, encolor, presupuesto, nomestudio, idproductor) 
- Elenco (t�tulo, a�o, nombre, sueldo) 
- Actor (nombre, direcci�n, telefono, fechanacimiento, sexo) 
- Productor (idproductor, nombre, direcci�n, tel�fono) 
- Estudio (nomestudio, direcci�n)

*/

-- El ingreso total recibido por cada actor, sin importar en cuantas pel�culas haya participado.
SELECT nombre, SUM(sueldo)
FROM elenco
GROUP BY nombre

-- El monto total destinado a pel�culas por cada Estudio Cinematogr�fico, durante la d�cada de los 80's.
SET DATEFORMAT DMY
SELECT nomestudio, SUM(presupuesto)
FROM pelicula
WHERE a�o Between '01/01/1980' AND '31/12/1989'
GROUP BY nomestudio

-- Nombre y sueldo promedio de los actores (s�lo hombres) que reciben en promedio un pago superior a 5 millones de dolares por pel�cula.
SELECT nombre, AVG(sueldo) as 'Promedio sueldo'
FROM Elenco E, Actor A
WHERE E.nombre = A.nombre
AND sexo = 'masculino' 
GROUP BY A.nombre
HAVING AVG(sueldo) > 5000000

-- T�tulo y a�o de producci�n de las pel�culas con menor presupuesto. (Por ejemplo, la pel�cula de Titanic se ha producido en varias veces entre la lista de pel�culas estar�a la producci�n de Titanic y el a�o que fue filmada con menor presupuesto.)

SELECT p.titulo, p.a�o, pr.pres
FROM pelicula p, (SELECT titulo, MIN(presupuesto) as pres
                  FROM Pelicula
                  GROUP BY titulo) pr
WHERE p.titulo = pr.titulo
  AND p.presupuesto = pres


--Mostrar el sueldo de la actriz mejor pagada.
SELECT MAX(sueldo)
FROM Elenco E, Actor A
WHERE E.nombre = A.nombre
  AND A.sexo = 'femenino'

