Ejercicio8.sql


-- 1.- Actrices de �Las brujas de Salem�.


    -- Sin Subconsultas
    SELECT nombre
    FROM Elenco E, Actor A
    WHERE E.nombre = A.nombre
    AND sexo = 'Femenino'
    AND titulo = 'Las brujas de Salem'
    
    -- Con Subconsultas
    
    SELECT nombre
    FROM Actor A
    WHERE nombre IN (SELECT nombre
                    FROM Elenco
                    WHERE titulo = 'Las brujas de Salem')
    AND sexo = 'Femenino'
  
-- 2.- Nombres de los actores que aparecen en pel�culas producidas por MGM en 1995. 


-- Sin Subconsultas
SELECT nombre
FROM Elenco E, Pelicula P
WHERE E.titulo = P.titulo
  AND E.a�o = P.a�o
  AND P.nomestudio = 'MGM'
  AND a�o = 1995


-- Con Subconsultas
SELECT nombre
FROM Elenco E, (SELECT titulo, a�o
               FROM Pelicula P
               WHERE P.nomestudio = 'MGM'
               AND a�o = 1995) P
WHERE E.titulo = P.titulo
  AND E.a�o = P.a�o


 
-- 3.- Pel�culas que duran m�s que �Lo que el viento se llev� (de 1939).


    -- Sin Subconsultas
    
    -- NO existe forma de hacerlo
    
    -- Con Subconsultas 
    SELECT titulo
    FROM Peliculas P, 
    WHERE duraci�n > (SELECT duraci�n 
                      FROM Pelicula
                      WHERE titulo = 'Lo que el viento se llev�'
                      AND a�o = 1939)
                  
-- 4.- Productores que han hecho m�s pel�culas que George Lucas.
    
    -- Sin Subconsultas
    
    --NO existe forma de hacerlo
    
    -- Con Subconsultas 
    
    SELECT nombre
    FROM Pelicula Pe, Productor P
    WHERE Pe.idproductor = P.idproductor
    GROUP BY Pe.idproductor
    HAVING COUNT(*) > (SELECT COUNT(*)
                        FROM Pelicula Pe, Productor P
                        WHERE Pe.idproductor = P.idproductor
                        AND P.nombre = 'George Lucas' 
                        )




-- 5.- Nombres de los productores de las pel�culas en las que ha aparecido Sharon Stone.


-- Sin Subconsultas
SELECT P.nombre
FROM Pel�cula Pe, Productor P, Elenco E
WHERE Pe.idproductor = P.idproductor
      AND Pe.titulo = E.titulo
      AND Pe.a�o = E.a�o
      AND E.nombre = 'Sharon Stone'
      
-- Con Subconsultas
SELECT P.nombre
FROM Productor P, (SELECT idproductor
                    FROM Pel�cula Pe, Elenco E 
                    WHERE Pe.titulo = E.titulo
                      AND Pe.a�o = E.a�o
                      AND E.nombre = 'Sharon Stone') R
WHERE P.idproductor = R.idproductor




SELECT P.nombre
FROM Productor P
WHERE P.idproductor IN (SELECT idproductor
                        FROM Pel�cula Pe, Elenco E 
                        WHERE Pe.titulo = E.titulo
                        AND Pe.a�o = E.a�o
                        AND E.nombre = 'Sharon Stone')




/**
  Pel�cula (t�tulo, a�o, duraci�n, encolor, nomestudio, idproductor)
  Elenco (t�tulo, a�o, nombre)
  Actor (nombre, direcci�n, telefono, fechanacimiento, sexo)
  Productor (idproductor, nombre, direcci�n, tel�fono, importeventas)
  Estudio (nomestudio, direcci�n)
**/
-- 6.- T�tulo de las pel�culas que han sido filmadas m�s de una vez


-- Sin Subconsultas
SELECT titulo
FROM Pel�cula Pe
GROUP BY titulo
HAVING COUNT(*) > 1


-- Con Subconsultas
SELECT Pe.titulo
FROM Pelicula Pe, (SELECT titulo, COUNT(*) as total
                   FROM Pelicula 
                   GROUP BY titulo)
                   ) Q
WHERE Q.total > 1;


